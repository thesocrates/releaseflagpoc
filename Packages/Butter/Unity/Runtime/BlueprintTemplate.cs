﻿using System.IO;

namespace Butter.Unity.Runtime {

	public class BlueprintTemplate : Template {

		protected virtual string modelDBName => $"{modelName}DB";
		protected virtual string modelCamelName => $"{modelName.ToLower()}";
		protected virtual string databaseDir => "Assets/Database";
		protected virtual string dataPath => $"{databaseDir}/{modelDBName}";
		protected virtual string blueprintPath => $"{dataPath}/{modelName}Blueprint.cs";

		public override void GenerateCode() {
			GenerateBlueprint();
		}

		void GenerateBlueprint() {
			Directory.CreateDirectory(dataPath);
			if (File.Exists(blueprintPath)) { return; }

			var blueprintFile = $@"
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu (menuName = ""Blueprints/{modelName}"")]
public partial class {modelName}Blueprint : ScriptableObject {{
	[HideLabel, InlineProperty, ShowInInspector]
	public {modelName} {modelName} {{
		get => {modelCamelName};
		set => {modelCamelName} = value;
	}}

	[SerializeField, HideInInspector]
	{modelName} {modelCamelName};
}}";

			File.WriteAllText(blueprintPath, blueprintFile);
		}
	}

}