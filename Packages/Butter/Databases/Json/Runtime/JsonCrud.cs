﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System;

namespace Butter.Database.Json {

	public class JsonCrud<TModel> : IQueryable<TModel>, IFactory<TModel> where TModel : IModel, new() {
		public static string modelName => typeof(TModel).Name;
		public static string dbDirectory => UnityEngine.Application.streamingAssetsPath;
		public static string dbPath => $"{dbDirectory}/{modelName}DB.json";

		protected struct JsonData {
			[JsonProperty("models")]
			public List<TModel> models;
			public JsonData(List<TModel> models) {
				this.models = models;
			}
		}

		public async UniTask<TModel> Create(string name, CancellationToken cancellationToken) {
			Directory.CreateDirectory(dbDirectory);
			if (string.IsNullOrEmpty(name)) {
				name = $"{modelName}{Guid.NewGuid().ToString().Replace("-", "")}";
			}

			var list = await All(cancellationToken) ?? new List<TModel>();
			if (list.Exists(x => x.Name == name)) {
				UnityEngine.Debug.LogError($"Can't create {modelName} called {name} bc it already exists");
				return list.FirstOrDefault(x => x.Name == name);
			}
			var model = new TModel() { Name = name };
			list.Insert(0, model);
			await ReplaceAll(list, cancellationToken);
			name = string.Empty;
			return model;
		}

		public virtual async UniTask<List<TModel>> All(CancellationToken cancellationToken) {
			if (!File.Exists(dbPath)) { return new List<TModel>(); }
			var db = File.ReadAllText(dbPath);
			var json = JsonConvert.DeserializeObject<JsonData>(db);
			return json.models;
		}

		public virtual async UniTask<bool> Delete(string id, CancellationToken cancellationToken) {
			var all = await All(cancellationToken);
			all.RemoveAll(x => x.Name == id);
			await ReplaceAll(all, cancellationToken);
			return true;
		}

		public virtual async UniTask<TModel> Get(string id, CancellationToken cancellationToken) {
			var all = await All(cancellationToken);
			return all.FirstOrDefault(model => model.Name == id);
		}

		public virtual async UniTask<bool> Update(string id, TModel model, CancellationToken cancellationToken) {
			var all = await All(cancellationToken);
			int i = all.IndexOf(await Get(id, cancellationToken));
			if (i < 0) { return false; }
			all[i] = model;
			await ReplaceAll(all, cancellationToken);
			return true;
		}

		public virtual async UniTask ReplaceAll(List<TModel> models, CancellationToken token) {
			Directory.CreateDirectory(dbDirectory);
			var db = new JsonData(models);
			var json = JsonConvert.SerializeObject(db, Formatting.Indented);
			File.WriteAllText(dbPath, json);
#if UNITY_EDITOR
			UnityEditor.AssetDatabase.Refresh();
#endif
		}
	}
}