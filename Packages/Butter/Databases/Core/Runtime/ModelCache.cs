﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Butter.Database {
	public class ModelCache<TModel> where TModel : IModel {
		private Dictionary<string, TModel> dictionary;
		public bool isLoaded;
		public IQueryable<TModel> Queryable { get; set; }
		public CancellationTokenSource TokenSource { get; set; }

		public ModelCache(IQueryable<TModel> queryable, CancellationTokenSource tokenSource) {
			this.Queryable = queryable;
			this.TokenSource = tokenSource;
		}

		public async UniTaskVoid Initialize() {
			isLoaded = false;
			var allModels = await Queryable.All(TokenSource.Token);
			dictionary = new Dictionary<string, TModel>();
			foreach (var model in allModels) {
				dictionary.Add(model.Name, model);
			}
			isLoaded = true;
		}
		public async UniTask IsDoneLoading() {
			while (!isLoaded) {
				await UniTask.Yield();
			}
		}
		public TModel GetSync(string id) {
			if (!isLoaded) {
				throw new ModelNotLoadedException("Tried to access {FooBlueprintId.Bimbobs} before loading was completed.");
			}
			return dictionary[id];
		}
	}
}