﻿#if ODIN_INSPECTOR
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using UnityEditor;

namespace Butter.Unity.Editor {
	public class ModelDrawer<T> : OdinValueDrawer<T> where T : IModel {

		protected override void DrawPropertyLayout(GUIContent label) {

			Rect rect = EditorGUILayout.GetControlRect();
			IModel value = this.ValueEntry.SmartValue;
			try {
				if (label != null) {
					label.text = value.Name;
					rect = EditorGUI.PrefixLabel(rect, label);
				}

				EditorGUI.LabelField(rect, value.Name, EditorStyles.boldLabel);
			} catch (System.Exception e) {
				if (e is MissingReferenceException || e is System.NullReferenceException) {
					return;
				}
				throw;
			}
			this.CallNextDrawer(label);
		}

	}
}
#endif
