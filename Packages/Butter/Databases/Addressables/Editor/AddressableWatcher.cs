﻿using System.IO;
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets;
using Butter.Database.Editor;

namespace Butter.Database.UnityAddressables.Editor {
	public class AddressableWatcher<TModel> : ModelWatcher<TModel> where TModel : UnityEngine.ScriptableObject, IModel {
		protected override string path => Path.GetFullPath("Assets/");
		protected override string fileExt => ".asset";

		public override void Watch() {
			base.Watch();
			OnCreated += AddAddressable;
			OnChanged += UpdateAddressable;
			OnRenamed += UpdateAddressable;
			OnDeleted += DeleteAddressable;
			OnAny += GenerateAutocomplete;
		}
		private void AddAddressable(object source, FileSystemEventArgs e) {
			var assetPath = e.FullPath.Split(new string[] { "Assets/" }, System.StringSplitOptions.RemoveEmptyEntries)[1];
			assetPath = "Assets/" + assetPath;
			var label = typeof(TModel).Name;
			var assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
			foreach (var asset in assets) {
				if (asset.GetType() == typeof(TModel)) {
					TModel tAsset = (TModel)asset;
					AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
					var group = settings.FindGroup(label);
					if (group == null) {
						group = settings.CreateGroup(label, false, false, true, settings.DefaultGroup.Schemas);
						settings.SetDirty(AddressableAssetSettings.ModificationEvent.GroupAdded, group, true);
					}
					settings.AddLabel(label, true);
					var guid = AssetDatabase.AssetPathToGUID(assetPath);
					var entry = settings.CreateOrMoveEntry(guid, group);
					entry.address = $"Database/{label}/{tAsset.name}";
					entry.labels.Add(label);
					settings.CreateOrMoveEntry(entry.guid, group);
					settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryCreated, entry, true);
					settings.SetDirty(AddressableAssetSettings.ModificationEvent.LabelAdded, label, true);
					AssetDatabase.SaveAssets();
					_ = AddressablesHelpers.RefreshAddressables();
				}
			}
		}
		private void UpdateAddressable(object source, FileSystemEventArgs e) {
			var assetPath = e.FullPath.Split(new string[] { "Assets/" }, System.StringSplitOptions.RemoveEmptyEntries)[1];
			assetPath = "Assets/" + assetPath;
			var label = typeof(TModel).Name;
			var assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
			foreach (var asset in assets) {
				if (asset.GetType() == typeof(TModel)) {
					TModel tAsset = (TModel)asset;
					AddressableAssetSettings settings = AddressableAssetSettingsDefaultObject.Settings;
					var guid = AssetDatabase.AssetPathToGUID(assetPath);
					var entry = settings.FindAssetEntry(guid);
					var targetAddress = $"Database/{label}/{tAsset.name}";
					if (entry.address != targetAddress) {
						entry.address = targetAddress;
						settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryModified, entry, true);
						AssetDatabase.SaveAssets();
						_ = AddressablesHelpers.RefreshAddressables();
					}
				}
			}
		}
		private void DeleteAddressable(object source, FileSystemEventArgs e) {
			_ = AddressablesHelpers.RefreshAddressables();
		}
		private void GenerateAutocomplete(object source, FileSystemEventArgs e) {
			_ = GenerationHelpers.GenerateIds<TModel>(crud, modelName, directoryPath);
			_ = GenerationHelpers.GenerateCache<TModel>(crud, modelName, directoryPath);
		}
		//where do we put this
		protected AddressableCrud<TModel> crud = new AddressableCrud<TModel>();
		protected string modelName => typeof(TModel).Name;
		protected string directoryPath => $"Assets/Database/{modelName}/";
		//
	}
}
