﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;

namespace Butter.Database.Editor {
	public abstract class ModelWatcher<TModel> where TModel : IModel {
		protected abstract string path { get; }
		protected abstract string fileExt { get; }
		protected FileSystemWatcher watcher;
		protected event FileSystemEventHandler OnCreated;
		protected event RenamedEventHandler OnRenamed;
		protected event FileSystemEventHandler OnChanged;
		protected event FileSystemEventHandler OnDeleted;
		protected event FileSystemEventHandler OnAny;
		protected Queue<Action> actions;

		public virtual void Watch() {
			watcher = new FileSystemWatcher(path);
			watcher.Filter = $"*{fileExt}";
			watcher.IncludeSubdirectories = true;
			watcher.Created += OnCreatedInternal;
			watcher.Deleted += OnDeletedInternal;
			watcher.Renamed += OnRenamedInternal;
			watcher.Changed += OnChangedInternal;
			watcher.EnableRaisingEvents = true;
			actions = new Queue<Action>();

			EditorApplication.update -= Update;
			EditorApplication.update += Update;
		}
		private void OnCreatedInternal(object source, FileSystemEventArgs e) {
			actions.Enqueue(() => {
				OnCreated?.Invoke(source, e);
				OnAny?.Invoke(source, e);
			});
		}
		private void OnChangedInternal(object source, FileSystemEventArgs e) {
			actions.Enqueue(() => {
				OnChanged?.Invoke(source, e);
				OnAny?.Invoke(source, e);
			});
		}
		private void OnRenamedInternal(object source, RenamedEventArgs e) {
			actions.Enqueue(() => {
				OnRenamed?.Invoke(source, e);
				OnAny?.Invoke(source, e);
			});
		}
		private void OnDeletedInternal(object source, FileSystemEventArgs e) {
			actions.Enqueue(() => {
				OnDeleted?.Invoke(source, e);
				OnAny?.Invoke(source, e);
			});
		}
		private void Update() {
			lock (actions) {
				while (actions.Count > 0) {
					actions.Dequeue()?.Invoke();
				}
			}
		}
	}
}