﻿using System;

namespace Butter.Database {
	public class ModelNotLoadedException : Exception {
		public ModelNotLoadedException() {
		}
		public ModelNotLoadedException(string message)
			: base(message) {
		}
		public ModelNotLoadedException(string message, Exception inner)
			: base(message, inner) {
		}
	}
}