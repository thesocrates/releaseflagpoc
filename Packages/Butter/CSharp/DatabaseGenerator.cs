﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Butter {
	public static class DatabaseGenerator {

		public static void GenerateDatabases() {

			var typesWithMyAttribute = GetAllWithAttribute<Template>();

			foreach (var item in typesWithMyAttribute) {
				Type type = item.type;
				bool isValid = typeof(IModel).IsAssignableFrom(type);
				if (!isValid) {
					throw new NotAModelException($"Database generation for {type.ToString()} failed because it does not implement the IModel interface.");
				}
				foreach (var template in item.attributes) {
					GenerateDatabaseFor(type, template);
				}
			}
		}

		public static void GenerateDatabaseFor(Type type, Template template) {
			if (string.IsNullOrEmpty(template.modelName)) {
				template.SetModelName(type.Name.ToString());
			}
			template.GenerateCode();
		}

		public static List<TypeAndAttributes<T>> GetAllWithAttribute<T>() where T : Attribute {
			var typesWithMyAttribute =
				from a in AppDomain.CurrentDomain.GetAssemblies()
				from t in a.GetTypes().AsParallel()
				let attributes = t.GetCustomAttributes(typeof(T), true)
				where attributes != null && attributes.Length > 0
				select new TypeAndAttributes<T>(t, attributes.Cast<T>());
			return typesWithMyAttribute.ToList();
		}
	}
}