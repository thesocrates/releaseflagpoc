﻿using System;

namespace Butter {
	public class NotAModelException : Exception {
		public NotAModelException() {
		}
		public NotAModelException(string message)
			: base(message) {
		}
		public NotAModelException(string message, Exception inner)
			: base(message, inner) {
		}
	}
}