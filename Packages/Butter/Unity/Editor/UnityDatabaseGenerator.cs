﻿using UnityEditor;

namespace Butter.Unity.Editor {

	public static class UnityDatabaseGenerator {

		[MenuItem("Database/Generate %#G", false, -1)]
		public static void GenerateDatabases() {

			var sw = new System.Diagnostics.Stopwatch();
			sw.Start();

			DatabaseGenerator.GenerateDatabases();
			AssetDatabase.Refresh();

			sw.Stop();
			UnityEngine.Debug.Log($"Database generation complete in {sw.ElapsedMilliseconds}ms");

		}

	}

}