#if UNITY_EDITOR
using Butter.Database;
using UnityEditor;

public class FooBlueprintWindow : AdminWindow<FooBlueprint, FooBlueprintAdmin> {
	[MenuItem("Database/FooBlueprintWindow")]
	public static void OpenWindow() {
		GetWindow<FooBlueprintWindow>().Show();
	}
}
#endif