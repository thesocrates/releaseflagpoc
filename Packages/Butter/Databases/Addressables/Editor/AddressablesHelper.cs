﻿using System;
using System.Reflection;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.Util;

public class AddressablesHelpers {
	[MenuItem("Tools/RefreshAddressables")]
	public async static UniTask RefreshAddressables() {
		await Addressables.InitializeAsync();
		var assembly = Assembly.GetAssembly(typeof(Addressables));
		var addressablesType = assembly.GetType("UnityEngine.AddressableAssets.Addressables");
		var addressablesImplType = assembly.GetType($"UnityEngine.AddressableAssets.AddressablesImpl");
		var newImpl = Activator.CreateInstance(
			addressablesImplType,
			new object[] { new LRUCacheAllocationStrategy(1000, 1000, 100, 10) })
			;
		var privateStaticBindingFlags = BindingFlags.NonPublic | BindingFlags.Static;
		var m_addressablesInstance = addressablesType.GetField("m_AddressablesInstance", privateStaticBindingFlags).GetValue(null);
		addressablesType.GetField("m_AddressablesInstance", privateStaticBindingFlags).SetValue(m_addressablesInstance, newImpl);
	}
}