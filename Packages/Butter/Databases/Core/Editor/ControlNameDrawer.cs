﻿using Sirenix.OdinInspector.Editor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Internal;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEngine;
using System.Reflection;
using System.Linq;
using System;
using System.Collections;

[CustomPropertyDrawer(typeof(ControlNameAttribute))]
public class ControlNameDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		ControlNameAttribute returnFunction = attribute as ControlNameAttribute;
		var controlName = returnFunction.controlName;
		GUI.SetNextControlName(controlName);
		EditorGUI.PropertyField(position, property, label);
		//base.OnGUI(position, property, label);
		//var obj = GetTargetObjectWithProperty(property);
		// Debug.Log(obj.GetType());
		// var otherObj = Convert.ChangeType(obj, obj.GetType());
		// var method = obj.GetType().GetMethod("OnChanged");
		// Debug.Log(method);


		// var selectedControlName = GUI.GetNameOfFocusedControl();
		// bool controlIsSelected = controlName == selectedControlName;

		// if (Event.current.keyCode == KeyCode.Return && controlIsSelected) {
		// 	var type = GetParent(property);
		// 	Debug.Log(type);
		// 	// var name = sanitizedName;
		// 	// var all = await Queries.All(TokenSource.Token);
		// 	// if (!all?.Exists(x => x.Name == name) ?? true) {
		// 	// 	await Create(TokenSource.Token);
		// 	// }

		// }

	}

	// 	/// <summary>
	// 	/// Gets the object that the property is a member of
	// 	/// </summary>
	// 	/// <param name="property"></param>
	// 	/// <returns></returns>
	// 	public static object GetTargetObjectWithProperty(SerializedProperty property) {
	// 		string path = property.propertyPath.Replace(".Array.data[", "[");
	// 		object obj = property.serializedObject.targetObject;
	// 		string[] elements = path.Split('.');

	// 		for (int i = 0; i < elements.Length - 1; i++) {
	// 			string element = elements[i];
	// 			if (element.Contains("[")) {
	// 				string elementName = element.Substring(0, element.IndexOf("["));
	// 				int index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
	// 				obj = GetValue_Imp(obj, elementName, index);
	// 			} else {
	// 				obj = GetValue_Imp(obj, element);
	// 			}
	// 		}

	// 		return obj;
	// 	}

	// 	private static object GetValue_Imp(object source, string name) {
	// 		if (source == null) {
	// 			return null;
	// 		}

	// 		Type type = source.GetType();

	// 		while (type != null) {
	// 			FieldInfo field = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
	// 			if (field != null) {
	// 				return field.GetValue(source);
	// 			}

	// 			PropertyInfo property = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
	// 			if (property != null) {
	// 				return property.GetValue(source, null);
	// 			}

	// 			type = type.BaseType;
	// 		}

	// 		return null;
	// 	}

	// 	private static object GetValue_Imp(object source, string name, int index) {
	// 		IEnumerable enumerable = GetValue_Imp(source, name) as IEnumerable;
	// 		if (enumerable == null) {
	// 			return null;
	// 		}

	// 		IEnumerator enumerator = enumerable.GetEnumerator();
	// 		for (int i = 0; i <= index; i++) {
	// 			if (!enumerator.MoveNext()) {
	// 				return null;
	// 			}
	// 		}

	// 		return enumerator.Current;
	// 	}
}