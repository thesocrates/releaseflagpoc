﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace Butter.Database {
	public interface IQueryable<TModel> where TModel : IModel {
		UniTask<bool> Update(string id, TModel model, CancellationToken cancellationToken);
		UniTask<bool> Delete(string id, CancellationToken cancellationToken);
		UniTask<TModel> Get(string id, CancellationToken cancellationToken);
		UniTask<List<TModel>> All(CancellationToken cancellationToken);
	}
}