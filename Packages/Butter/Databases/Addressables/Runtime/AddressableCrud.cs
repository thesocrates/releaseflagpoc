﻿using System.Collections.Generic;
using System.IO;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
#endif

namespace Butter.Database.UnityAddressables {

	public class AddressableCrud<TModel> : IQueryable<TModel>, IFactory<TModel> where TModel : ScriptableObject, IModel {
		public static string modelName => typeof(TModel).Name;
		public static string modelAddressPrefix => $"Database/{modelName}/";
		public static string dbDirectory => $"Assets/Blueprints/{modelName}/";

		public async UniTask<List<TModel>> All(CancellationToken cancellationToken) {

#if UNITY_EDITOR
			if (!Application.isPlaying) {
				return await GetAllInEditMode(cancellationToken);
			}
#endif

			var result = await Addressables.LoadResourceLocationsAsync(modelName);
			if (result.Count == 0) {
				return new List<TModel>();
			}
			return (List<TModel>)await Addressables.LoadAssetsAsync<TModel>(modelName, null);
		}

		public async UniTask<TModel> Create(string name, CancellationToken cancellationToken) {
#if UNITY_EDITOR
			Directory.CreateDirectory(dbDirectory);
			var asset = ScriptableObject.CreateInstance<TModel>();
			var path = $"{dbDirectory}{name}.asset";
			UnityEditor.AssetDatabase.CreateAsset(asset, path);
			UnityEditor.AssetDatabase.SaveAssets();
			return asset;
#else
			return null;
#endif
		}

		public async UniTask<bool> Delete(string id, CancellationToken cancellationToken) {
#if UNITY_EDITOR
			var model = await Get(id, cancellationToken);
			if (model == null) { return false; }
			var path = UnityEditor.AssetDatabase.GetAssetPath(model);
			UnityEditor.AssetDatabase.DeleteAsset(path);
			return true;
#else
            return false;
#endif
		}

		public async UniTask<TModel> Get(string id, CancellationToken cancellationToken) {
#if UNITY_EDITOR
			if (!Application.isPlaying) {
				return await GetInEditMode(id, cancellationToken);
			}
#endif
			return await Addressables.LoadAssetAsync<TModel>($"{modelAddressPrefix}{id}");
		}

		public async UniTask<bool> Update(string id, TModel model, CancellationToken cancellationToken) {
			throw new System.NotImplementedException();
		}

		//please god kill me
#if UNITY_EDITOR
		public async UniTask<List<TModel>> GetAllInEditMode(CancellationToken cancellationToken) {
			var settings = AddressableAssetSettingsDefaultObject.Settings;
			if (settings?.groups == null) {
				UnityEngine.Debug.LogWarning($"Addressables groups do not exist yet. ");
				return null;
			}
			List<AddressableAssetEntry> allEntries = new List<AddressableAssetEntry>(settings.groups.SelectMany(g => g.entries));
			allEntries = allEntries.Where(e => e.labels.Contains(modelName)).ToList();
			List<TModel> all = new List<TModel>();
			foreach (var entry in allEntries) {
				all.Add(UnityEditor.AssetDatabase.LoadAssetAtPath<TModel>(entry.AssetPath));
			}
			return all;
		}
		public async UniTask<TModel> GetInEditMode(string id, CancellationToken cancellationToken) {
			var settings = AddressableAssetSettingsDefaultObject.Settings;
			List<AddressableAssetEntry> allEntries = new List<AddressableAssetEntry>(settings.groups.SelectMany(g => g.entries));
			var entry = allEntries.First(e => e.address == $"{modelAddressPrefix}{id}");
			if (entry == null) { return null; }
			var model = UnityEditor.AssetDatabase.LoadAssetAtPath<TModel>(entry.AssetPath);
			return model;
		}
#endif
	}
}