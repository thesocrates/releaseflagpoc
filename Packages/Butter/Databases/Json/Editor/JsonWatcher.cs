using System.IO;
using Butter.Database.Editor;

namespace Butter.Database.Json.Editor {
	public class JsonWatcher<TModel> : ModelWatcher<TModel> where TModel : IModel, new() {
		protected override string path => Path.GetFullPath(JsonCrud<TModel>.dbDirectory);
		protected override string fileExt => ".json";

		public override void Watch() {
			base.Watch();
			OnChanged += GenerateAutocomplete;
		}

		private void GenerateAutocomplete(object source, FileSystemEventArgs e) {
			_ = GenerationHelpers.GenerateIds<TModel>(crud, modelName, directoryPath);
			_ = GenerationHelpers.GenerateCache<TModel>(crud, modelName, directoryPath);
		}
		//where do we put this
		protected JsonCrud<TModel> crud = new JsonCrud<TModel>();
		protected string modelName => typeof(TModel).Name;
		protected string directoryPath => $"Assets/Database/{modelName}/";
		//
	}
}